# Demonstration
## Demo 1 - Execute change log to update db
Config:
```
changeLogFile=db/changelog/db.changelog-master.xml
url=jdbc:postgresql://localhost:5440/demo
username=postgres
password=postgres
driver=org.postgresql.Driver
```
Command:
```
mvn liquibase:update
```


## Demo 2 - Generate a ChangeLog From an Existing Database
Config:
```
changeLogFile=db/changelog/db.changelog-master.xml
url=jdbc:postgresql://localhost:5440/demo
username=postgres
password=postgres
driver=org.postgresql.Driver
outputChangeLogFile=src/main/resources/db/output/generated-outputChangeLog.xml
```
Command:
```
mvn liquibase:generateChangeLog
```



## Demo 3 - Execute change log to rollback db
Config:
```
changeLogFile=db/changelog/db.changelog-master.xml
url=jdbc:postgresql://localhost:5440/demo
username=postgres
password=postgres
driver=org.postgresql.Driver
```
Command:
```
mvn liquibase:rollback -Dliquibase.rollbackTag=1.0
```
